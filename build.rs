extern crate chrono;

use chrono::Local;
use std::process::Command;

fn main() {
    let date = Local::now();
    println!("cargo:rustc-env=MARUSKA_DATE={}", date.format("%Y-%m-%d"));
    let output = Command::new("git")
        .args(&["rev-parse", "--short", "HEAD"])
        .output()
        .unwrap();
    let commit = String::from_utf8(output.stdout).unwrap();
    println!("cargo:rustc-env=MARUSKA_COMMIT={}", commit);
}
