# maruska

Maruska is a terminal client for <https://marietje-zuid.science.ru.nl>.

## Getting started

```shell
# Setup rust toolchain if you haven't already
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

# Install maruska
cargo install --git https://gitlab.science.ru.nl/dsprenkels/maruska.git

# Run maruska
~/.cargo/bin/maruska
```

## Uninstall

```shell
cargo uninstall maruska
```

## Upgrade

Cargo currently has no support for "regular" upgrading of installed packages.
You can force a reinstall, however, using:

## Security

A user running maruska can request the help screen.  `man` will use the system
preferred pager program (set by `$PAGER` and `$MANPAGER`).  If this is set to
`less`, the user can execute any command by default.

If you intend to sandbox this program, to allow usage for anonymous users, make
sure that you _completely_ sandbox the process tree and file system. The user
can run arbitrary code through the maruska process, and maruska is not designed
to provide any security to prevent this.

```shell
cargo install --force --git https://gitlab.science.ru.nl/dsprenkels/maruska.git
```