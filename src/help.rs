use anyhow::Error;
use std::io::Write;
use std::process;

use crate::data::*;

const MANUAL: &[u8] = include_bytes!("../man/maruska.1");

pub fn show_help(model: &mut Model) {
    if let Err(err) = show_help_inner() {
        log::error!("{:?}", err);
        let msg = format!("could not show help: {}", err);
        crate::tui::set_error(model, msg);
    }
    model.tui.clear();
}

fn show_help_inner() -> Result<(), Error> {
    let mut command = process::Command::new("man")
        .args(&["--local-file", "-"])
        .stdin(process::Stdio::piped())
        .spawn()?;
    let mut stdin = command.stdin.take().expect("stdin has not been captured");
    stdin.write_all(MANUAL)?;
    drop(stdin);
    let _ = command.wait()?;
    Ok(())
}
