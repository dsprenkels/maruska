use anyhow::{bail, Context, Error};
use lazy_static::lazy_static;
use sha2::Digest;
use std::fs;
use std::io::{BufReader, BufWriter};
use std::sync::mpsc;
use std::time::{Duration, SystemTime};

use crate::client;
use crate::data::*;

const SONG_CACHE_MAX_AGE: u64 = 7 * 24 * 60 * 60;

lazy_static! {
    static ref SONG_CACHE_FILENAME: String = {
        let host = &*client::HOST;
        let hash = sha2::Sha256::digest(host.as_bytes());
        let mut hex = String::new();
        for b in hash.iter().take(4) {
            hex.push_str(&format!("{:02x}", b));
        }
        format!("song_cache-{}.json", &hex)
    };
}

pub type SearchStub = String;
pub type SearchCache = Vec<SearchCacheSong>;

#[derive(Debug, Default)]
pub struct SearchCacheSong {
    pub song_idx: SongIdx,
    pub artist: crate::search::SearchStub,
    pub title: crate::search::SearchStub,
    pub uploader: crate::search::SearchStub,
}

pub type SearchResults = Vec<SearchResultsEpoch>;

#[derive(Clone, Debug)]
pub struct SearchResultsEpoch {
    /// The length of this query.  Used to determine how a new query was
    /// updated.
    query_len: usize,
    /// Automaton describing this query.
    automaton: aho_corasick::AhoCorasick,
    /// The list of songs that we found for this query (by SongIdx).
    pub results: Vec<StubIdx>,
    /// The index where we stopped searching in the last epoch.
    /// When `left_at >= prev_epoch.results.len()` <=> for this query, we
    /// searched through the complete last list.
    left_at: usize,
}

/// Move the cursor relative to its current position.
///
/// This function will update the search results accordingly.  The new
/// position of the cursor may be invalid, and in that case it will be
/// automatically fixed.
pub fn move_cursor(model: &mut Model, by: isize) {
    if by.is_negative() {
        let cursor = model.search_cursor.saturating_sub(by.abs() as usize);
        model.search_cursor = cursor;
    } else {
        model.search_cursor += by as usize;
        update_search_results(model);
    }
}

/// Move the cursor to an absolute position.
///
/// This function will update the search results accordingly.  The new
/// position of the cursor may be invalid, and in that case it will be
/// automatically fixed.
pub fn set_cursor(model: &mut Model, cursor: usize) {
    model.search_cursor = cursor;
    update_search_results(model);
}

/// Fix the location of the cursor to be in bounds.
fn fixup_cursor(model: &mut Model) {
    let count = model
        .search_results
        .last()
        .map(|sre| sre.results.len())
        .unwrap_or(0);
    if count == 0 {
        model.search_cursor = 0;
    } else {
        model.search_cursor = std::cmp::min(count - 1, model.search_cursor);
    }
}

/// Build a search cache from `songs`.
pub fn build_search_strings(songs: &[Song]) -> Vec<SearchCacheSong> {
    let tick = SystemTime::now();
    let mut sc = Vec::with_capacity(songs.len());
    for (idx, song) in songs.iter().enumerate() {
        let mut scs = SearchCacheSong {
            song_idx: SongIdx(idx),
            ..Default::default()
        };
        if let Some(title) = song.title {
            scs.title = stubbify_str(title);
        }
        if let Some(artist) = song.artist {
            scs.artist = stubbify_str(artist);
        }
        if let Some(uploader) = song.uploader {
            scs.uploader = stubbify_str(uploader);
        }
        sc.push(scs);
    }
    let elapsed = tick.elapsed().unwrap_or_default().as_millis();
    log::debug!("building search index took {} ms", elapsed);
    sc
}

/// Return a byte-vec representing a simplified representation of `s`.
fn stubbify_str<T>(s: T) -> String
where
    T: Sized + AsRef<str>,
{
    let mut s = deunicode::deunicode(&s.as_ref());
    s.make_ascii_lowercase();
    s.retain(|c| c.is_alphanumeric());
    s
}

/// Clear the list of search results.
pub fn clear_search_results(model: &mut Model) {
    model.search_results.clear();
}

/// Lazily update the search results such that we have enough to fill the
/// terminal interface.
pub fn update_search_results(model: &mut Model) {
    let tick = SystemTime::now();
    let original_cursor_stub_idx = &model
        .search_results
        .last()
        .and_then(|sre| sre.results.get(model.search_cursor))
        .cloned();

    // Update the search results
    let requested_amount = crate::tui::search_results_requested_amount(model);
    log::trace!("requested_amount: {}", requested_amount);
    update_search_results_inner(
        &mut model.search_results,
        &model.search_cache,
        &model.query,
        requested_amount,
    );

    // Update the cursor to the location where it should be now
    let results = &model
        .search_results
        .last()
        .expect("no search results present, though we *just* did a search")
        .results;
    if let Some(stub_idx) = original_cursor_stub_idx {
        model.search_cursor = match results.binary_search(&stub_idx) {
            Ok(n) => n,
            Err(n) => n.saturating_sub(1),
        };
    }
    fixup_cursor(model);

    // Update the search results, because the cursor location has changed
    let requested_amount = crate::tui::search_results_requested_amount(model);
    update_search_results_inner(
        &mut model.search_results,
        &model.search_cache,
        &model.query,
        requested_amount,
    );
    log_search_stats(model, requested_amount, tick);
}

/// Update the search results with a new query. Search until we have at least
/// `amount` search results (or until we searched through the whole list).
fn update_search_results_inner(
    srs: &mut Vec<SearchResultsEpoch>,
    sc: &[SearchCacheSong],
    query: &str,
    requested_amount: usize,
) {
    // After a backspace (or other change that deletes characters) we have to
    // trash the search results that are not relevant anymore.  These are the
    // search results that were for queries that were longer than the current
    // query.
    loop {
        let mut pop = false;
        if let Some(sre) = srs.last() {
            pop = sre.query_len > query.len();
        }
        if pop {
            srs.pop();
            continue;
        }
        break;
    }

    // If a character was added, we need to construct a new automaton.
    let last_query_len = srs.last().map(|sre| sre.query_len);
    if last_query_len.map_or(true, |n| n < query.len()) {
        let mut patterns = Vec::new();
        for word in query.split_whitespace() {
            patterns.push(stubbify_str(word));
        }
        if patterns.is_empty() {
            // Add the empty string, otherwise we will not match anything
            patterns.push(Default::default());
        }
        log::trace!("search patterns: {:?}", patterns);
        let mut build = aho_corasick::AhoCorasickBuilder::new();
        let automaton = build.dfa(true).build(&patterns);
        let epoch = SearchResultsEpoch {
            query_len: query.len(),
            automaton,
            results: Vec::new(),
            left_at: 0,
        };
        srs.push(epoch);
    }

    // Do the actual searching
    search_result_epoch_expand(srs.as_mut_slice(), sc, requested_amount);
    log::trace!("now have {} epochs for searching", srs.len());
    log::trace!(
        "last epoch has {} search results",
        srs.last().unwrap().results.len()
    );
}

/// Expand the result list for the slice of results in `srs`.  This function
/// will recursively call itself to fill the previous search result caches
/// as well.
///
/// # Returns
/// This function will return `true` if it knows that it has searched through
/// all the songs in the database.
fn search_result_epoch_expand(
    srs: &mut [SearchResultsEpoch],
    sc: &[SearchCacheSong],
    amount_requested: usize,
) -> bool {
    let (prev_epochs, next_epochs) = srs.split_at_mut(srs.len() - 1);
    let sre = &mut next_epochs[0];
    while sre.results.len() < amount_requested {
        if prev_epochs.is_empty() {
            // There is no previous epoch.  Search directly in the search
            // cache.
            let stub_idx = sre.left_at;
            let scs = if let Some(scs) = sc.get(stub_idx) {
                scs
            } else {
                // We reached the end of the song list
                return true;
            };
            if find_all_in_stub(&sre.automaton, scs) {
                sre.results.push(StubIdx(stub_idx));
            }
            sre.left_at += 1;
        } else {
            // There is a previous epoch.  Search only in the previous
            // search results.

            // Make sure the previous search results are caught up with the
            // current requested amount.
            let request_from_prev = sre
                .left_at
                .saturating_add(amount_requested - sre.results.len());
            let prev_reached_end = search_result_epoch_expand(prev_epochs, sc, request_from_prev);

            // Now search through the results of the previous epoch.
            let prev_sre = &prev_epochs.last().unwrap();
            for StubIdx(stub_idx) in &prev_sre.results[sre.left_at..] {
                let scs = sc
                    .get(*stub_idx)
                    .expect("song is in search results, but not in song cache");
                if find_all_in_stub(&sre.automaton, scs) {
                    sre.results.push(StubIdx(*stub_idx));
                }
                sre.left_at += 1;
            }

            if prev_reached_end {
                return true;
            }
        }
    }
    false
}

fn find_all_in_stub(automaton: &aho_corasick::AhoCorasick, scs: &SearchCacheSong) -> bool {
    let mut patterns_to_find = !(!0 << automaton.pattern_count());
    let artist = automaton.find_overlapping_iter(&scs.artist);
    let title = automaton.find_overlapping_iter(&scs.title);
    for m in artist.chain(title) {
        patterns_to_find &= !(1 << m.pattern());
        if patterns_to_find == 0 {
            return true;
        }
    }
    false
}

fn log_search_stats(model: &Model, requested_amount: usize, tick: SystemTime) {
    let elapsed = tick.elapsed().unwrap_or_default().as_micros();
    log::trace!(
        "search \"{}\" (with requested_amount={}) took {} µs",
        &model.query,
        requested_amount,
        elapsed
    );
    let sc = &model.search_cache;
    let left_at = model.search_results[0].left_at;
    let percent = 100. * left_at as f32 / (sc.len() as f32);
    log::trace!("searched {:.0}% through the song list", percent);
}

pub fn request_song(model: &mut Model) {
    try_recv_fetched_songs(model, true);

    // Get the song ID for the song that is currently under the cursor
    let song = if let Some(song) = get_match(model, model.search_cursor) {
        song
    } else {
        log::warn!(
            "user tried to request a song that could not be found (songs.len()={}, results.len()={}, cursor={}",
            model.all_songs.len(),
            model.search_results.len(),
            model.search_cursor,
        );
        let msg = "No song was selected!".to_string();
        crate::tui::set_error(model, msg);
        return;
    };

    let mut client = crate::client::Client::new();
    let song_id = song.id;
    let unknown = arrayvec::ArrayString::from("<unknown>").unwrap();
    let title = &song.title.unwrap_or_else(|| unknown);
    match client.request_song(song_id.expect("song has no id")) {
        Ok(()) => {
            let msg = format!("requested '{}'", title);
            crate::tui::set_ok(model, msg);
        }
        Err(err) => {
            let msg = format!("error: {}", err);
            crate::tui::set_error(model, msg);
        }
    }
}

/// Get the song at the n'th position in the search results, or return `None`
/// if there is none.
pub fn get_match(model: &Model, idx: usize) -> Option<&Song> {
    let sre = model.search_results.last()?;
    let stub_idx = *sre.results.get(idx)?;
    get_song_by_stub_idx(model, stub_idx)
}

/// Get a song from the search results by StubIdx.
pub fn get_song_by_stub_idx(model: &Model, stub_idx: StubIdx) -> Option<&Song> {
    let song_idx = model.search_cache.get(stub_idx.0)?.song_idx;
    get_song(model, song_idx)
}

/// Get a song from the search results by SongIdx.
pub fn get_song(model: &Model, song_idx: SongIdx) -> Option<&Song> {
    model.all_songs.get(song_idx.0)
}

/// Fetch the list of songs from disk (if is has previously been saved).
pub fn fetch_songs_from_filesystem(tx: &mpsc::SyncSender<SongFetchResult>) {
    log::info!("loading list of songs from filesystem");
    let tick = SystemTime::now();
    let all_songs: Vec<Song> = match load_songs_from_filesystem_inner() {
        Ok(songs) => songs,
        Err(err) => {
            log::info!("loading song list from filesystem failed: {}", err);
            let _ = tx.send(Err(err).context("could not load song list from disk"));
            return;
        }
    };
    let search_cache = build_search_strings(&all_songs);

    let elapsed = tick.elapsed().unwrap_or_default().as_millis();
    log::debug!(
        "loading list of songs from filesystem done ({} ms)",
        elapsed
    );
    let _ = tx.send(Ok((all_songs, search_cache)));
}

/// Load the file containing the song cache from disk.
///
/// This function is to be called from fetch_songs_from_filesystem.
fn load_songs_from_filesystem_inner() -> Result<Vec<Song>, Error> {
    let mut path = crate::cache_dir()?.to_owned();
    path.push(&*SONG_CACHE_FILENAME);

    let meta = fs::metadata(&path)?;
    if file_is_outdated(meta, Duration::from_secs(SONG_CACHE_MAX_AGE)) {
        let _ = fs::remove_file(&path);
        bail!("filesystem song cache is outdated");
    }

    let file = fs::File::open(path)?;
    let bufread = BufReader::new(file);
    Ok(serde_json::from_reader(bufread)?)
}

/// Check if this file is outdated, given some maximum age.
fn file_is_outdated(meta: fs::Metadata, max_age: Duration) -> bool {
    meta.modified()
        .ok()
        .and_then(|mtime| mtime.elapsed().ok())
        .map(|age| age >= max_age)
        .unwrap_or(true)
}

fn save_songs_to_filesystem(songs: &[Song]) -> Result<(), Error> {
    let tick = SystemTime::now();
    log::debug!("saving list of songs to filesystem");
    if let Err(err) = save_songs_to_filesystem_inner(songs) {
        log::warn!("saving list of songs to filesystem failed: {}", err);
    }
    let elapsed = tick.elapsed().unwrap_or_default().as_millis();
    log::debug!("saving list of songs to filesystem done ({} ms)", elapsed);
    Ok(())
}

fn save_songs_to_filesystem_inner(songs: &[Song]) -> Result<(), Error> {
    let mut path = crate::cache_dir()?.to_owned();
    path.push(&*SONG_CACHE_FILENAME);
    let file = fs::OpenOptions::new()
        .write(true)
        .truncate(true)
        .create(true)
        .open(path)?;
    let bufwrite = BufWriter::new(file);
    serde_json::to_writer(bufwrite, songs)?;
    Ok(())
}

/// Fetch the songs from the server.
pub fn fetch_songs_from_server(tx: &mpsc::SyncSender<SongFetchResult>) {
    log::info!("loading list of songs from server");
    let tick = SystemTime::now();
    let all_songs: Vec<Song> = match client::Client::new().get_all_songs() {
        Ok(songs) => songs,
        Err(err) => {
            log::warn!("loading song list from filesystem failed: {}", err);
            let _ = tx.send(Err(err).context("could not download song list"));
            return;
        }
    };
    let search_cache = build_search_strings(&all_songs);
    let elapsed = tick.elapsed().unwrap_or_default().as_millis();
    log::debug!("retrieving songs from server done ({} ms)", elapsed);

    let _ = save_songs_to_filesystem(&all_songs);
    let _ = tx.send(Ok((all_songs, search_cache)));
}

/// Receive the list of fetched songs from the background thread fetching them.
///
/// This function will use the channel receiver that is currently present in
/// the model. Set `block` to `true` if you want to wait for the background
/// thread to finish before returning.
pub fn try_recv_fetched_songs(model: &mut Model, block: bool) {
    if !model.all_songs.is_empty() && model.query_type == crate::query::QueryType::Search {
        log::trace!("refused to hotswap song list, because the user is currently in a search");
    }

    // If the background thread just finished fetching songs, make sure to
    // put them in the application model first.
    let mut recvd = None;
    if let Some(rx) = &mut model.fetch_songs_rx {
        recvd = if block {
            rx.recv().ok()
        } else {
            rx.try_recv().ok()
        };
    }
    if let Some(Ok((songs, search_cache))) = recvd {
        handle_fetched_songs(model, songs, search_cache);
    }
}

fn handle_fetched_songs(model: &mut Model, songs: Vec<Song>, cache: Vec<SearchCacheSong>) {
    if !model.all_songs.is_empty() {
        log::info!("hotswapping song list (new length: {})", songs.len());
    }
    model.all_songs = songs;
    model.search_cache = cache;

    clear_search_results(model);
    update_search_results(model);
    log::debug!("updating songs list");
    model.songs_loaded = true;
}
