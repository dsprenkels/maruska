use anyhow::{bail, Error};
use curl::easy::Easy;
use lazy_static::lazy_static;
use std::env;
use std::io::Write;
use std::path::PathBuf;
use std::sync::mpsc;
use std::time::SystemTime;
use thiserror::Error;

use crate::creds::Credentials;
use crate::data::*;

const COOKIE_NAME: &str = "csrftoken";
const DEFAULT_HOST: &str = "marietje.marie-curie.nl";

lazy_static! {
    pub static ref HOST: String = get_marietje_host();
    static ref COOKIE_FILE: PathBuf = {
        let mut path = PathBuf::from(crate::cache_dir().unwrap());
        path.push("cookies.txt");
        path
    };
}

#[derive(Error, Debug)]
#[error("http error 401 Unauthorized")]
pub struct UnauthorizedError(pub Option<String>);

#[derive(Error, Debug)]
#[error("http error 403 Unauthorized")]
pub struct ForbiddenError(pub Option<String>);

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
enum RequestType {
    Get,
    Post,
}

pub fn init() {
    curl::init();
}
#[derive(Debug)]
pub struct Client {
    handle: curl::easy::Easy,
}

impl Client {
    pub fn new() -> Client {
        Client {
            handle: new_easy_handle().expect("could not construct curl handle"),
        }
    }

    pub fn login(&mut self, creds: &Credentials) -> Result<(), Error> {
        let handle = &mut self.handle;
        let mut formdata = Vec::new();
        write!(
            formdata,
            "username={}&password={}",
            handle.url_encode(creds.username.as_bytes()),
            handle.url_encode(creds.password.as_bytes())
        )?;

        // Send the request
        let response = do_request(handle, RequestType::Post, &login_url(&*HOST), &formdata)?;
        log::trace!("login response: '{}'", String::from_utf8_lossy(&response));
        let status = handle.response_code()?;

        #[derive(serde::Deserialize, Debug)]
        struct Response {
            error: Option<String>,
        }

        let response: Response = serde_json::from_slice(&response)?;
        if status == 401 {
            // Login failed
            bail!(UnauthorizedError(response.error));
        }
        if status != 200 {
            bail!("bad status code during login: {}", status);
        }

        Ok(())
    }

    pub fn fetch_queue(&mut self) -> Result<Queue, Error> {
        let handle = &mut self.handle;
        let response = do_request(handle, RequestType::Get, &fetch_queue_url(&*HOST), &[])?;
        let status = handle.response_code()?;
        if status != 200 {
            bail!("bad status code during queue fetch: {}", status);
        }
        let queue = serde_json::from_slice(&response)?;
        Ok(queue)
    }

    pub fn get_all_songs(&mut self) -> Result<Vec<Song>, Error> {
        let handle = &mut self.handle;
        let response = do_request(
            handle,
            RequestType::Post,
            &load_songs_url(&*HOST),
            b"pagesize=100000",
        )?;
        let status = handle.response_code()?;
        if status != 200 {
            bail!("bad status code during song retrieval: {}", status);
        }

        #[derive(serde::Deserialize, Debug)]
        struct Response {
            current_page: Option<i32>,
            last_page: Option<i32>,
            data: Vec<Song>,
        }

        let response: Response = serde_json::from_slice(&response)?;
        log::debug!("downloaded {} songs", response.data.len());
        let current_page = response.current_page.unwrap_or(1);
        let last_page = response.last_page.unwrap_or(1);
        if current_page != last_page {
            log::warn!(
                concat!(
                    "last_page ({}) is not equal to current_page ({}); ",
                    "this suggests that we did not get all the songs from the database"
                ),
                last_page,
                current_page
            );
        }
        Ok(response.data)
    }

    pub fn request_song(&mut self, id: i32) -> Result<(), Error> {
        let handle = &mut self.handle;
        let mut formdata = arrayvec::ArrayVec::<[u8; 20]>::new();
        write!(formdata, "id={}", id).unwrap();
        let response = do_request(
            handle,
            RequestType::Post,
            &request_song_url(&*HOST),
            &formdata,
        )?;

        // If the status is not 200, return an error
        let status = handle.response_code()?;
        if status != 200 {
            log::warn!("bad status code after song request: {}", status);
        }

        #[derive(serde::Deserialize, Debug)]
        struct Response {
            success: bool,
            message: Option<String>,
        }

        let response: Response = serde_json::from_slice(&response)?;
        if !response.success {
            let message = response
                .message
                .unwrap_or_else(|| "unknown error".to_string());
            log::warn!("request unsuccessful: {}", message);
            bail!("{}", message);
        }
        Ok(())
    }

    pub fn skip_song(&mut self) -> Result<(), Error> {
        let handle = &mut self.handle;
        let _ = do_request(handle, RequestType::Post, &skip_song_url(&*HOST), &[])?;
        // Response is empty

        // If the status is not 200, return an error
        let status = handle.response_code()?;
        if status == 403 {
            // HTTP forbidden
            let msg = "skip action not allowed";
            bail!(ForbiddenError(Some(msg.to_owned())))
        } else if status != 200 {
            bail!("bad status code after skipping song: {}", status);
        }
        Ok(())
    }

    pub fn volume_up(&mut self) -> Result<(), Error> {
        let handle = &mut self.handle;
        let _ = do_request(handle, RequestType::Post, &volume_up_url(&*HOST), &[])?;
        // Response is empty

        // If the status is not 200, return an error
        let status = handle.response_code()?;
        if status == 403 {
            // HTTP forbidden
            let msg = "volume up not allowed";
            bail!(ForbiddenError(Some(msg.to_owned())))
        } else if status != 200 {
            bail!("bad status code after increasing volume: {}", status);
        }
        Ok(())
    }

    pub fn volume_down(&mut self) -> Result<(), Error> {
        let handle = &mut self.handle;
        let _ = do_request(handle, RequestType::Post, &volume_down_url(&*HOST), &[])?;
        // Response is empty

        // If the status is not 200, return an error
        let status = handle.response_code()?;
        if status == 403 {
            // HTTP forbidden
            let msg = "volume down not allowed";
            bail!(ForbiddenError(Some(msg.to_owned())))
        } else if status != 200 {
            bail!("bad status code after decreasing volume: {}", status);
        }
        Ok(())
    }

    pub fn mute(&mut self) -> Result<(), Error> {
        let handle = &mut self.handle;
        let _ = do_request(handle, RequestType::Post, &mute_url(&*HOST), &[])?;
        // Response is empty

        // If the status is not 200, return an error
        let status = handle.response_code()?;
        if status == 403 {
            // HTTP forbidden
            let msg = "toggling mute is not allowed";
            bail!(ForbiddenError(Some(msg.to_owned())))
        } else if status != 200 {
            bail!("bad status code after mute toggling: {}", status);
        }
        Ok(())
    }
}

fn do_request(
    handle: &mut Easy,
    request_type: RequestType,
    url: &str,
    formdata: &[u8],
) -> Result<Vec<u8>, Error> {
    ensure_has_csrftoken()?;

    let (tx, rx) = mpsc::channel();

    // Send the request
    log::trace!("initiating {:?} request to {}", request_type, url);
    handle.url(url)?;
    set_csrftoken_header(handle)?;
    match request_type {
        RequestType::Get => handle.get(true)?,
        RequestType::Post => handle.post(true)?,
    }
    if !formdata.is_empty() {
        log::trace!("formdata: '{}'", String::from_utf8_lossy(&formdata));
    }
    handle.post_fields_copy(formdata)?;
    handle.write_function(move |data| {
        tx.send(Vec::from(data)).expect("receiver vanished");
        Ok(data.len())
    })?;
    let tick = SystemTime::now();
    handle.perform()?;
    log::trace!(
        "response code: {} ({} ms)",
        handle.response_code().unwrap_or(!0),
        tick.elapsed().unwrap_or_default().as_millis()
    );

    // Accumulate the response
    let mut response = Vec::new();
    while let Ok(chunk) = rx.try_recv() {
        response.extend(chunk);
    }
    Ok(response)
}

fn new_easy_handle() -> Result<Easy, Error> {
    let mut handle = Easy::new();
    handle.referer(&referer(&*HOST)).unwrap();
    handle.follow_location(true).unwrap();
    handle.max_redirections(5).unwrap();
    handle.cookie_file(&*COOKIE_FILE).unwrap();
    handle.cookie_jar(&*COOKIE_FILE).unwrap();
    Ok(handle)
}

fn set_csrftoken_header(handle: &mut Easy) -> Result<(), Error> {
    let mut list = curl::easy::List::new();
    list.append(&format!("X-CSRFToken: {}", get_csrftoken()?.unwrap()))?;
    handle.http_headers(list)?;
    Ok(())
}

fn ensure_has_csrftoken() -> Result<(), Error> {
    if get_csrftoken().unwrap_or_default().is_none() {
        let mut handle = new_easy_handle()?;
        handle.url(&login_url(&*HOST)).unwrap();
        handle.get(true).unwrap();
        handle.perform()?;
    }
    if get_csrftoken()?.is_none() {
        bail!("got no csrftoken from the server");
    }
    Ok(())
}

fn get_csrftoken() -> Result<Option<String>, Error> {
    let cookies = std::fs::read_to_string(&*COOKIE_FILE)?;
    for cookie in cookies.split('\n') {
        let mut fields = cookie.split('\t');
        if fields.next().map(|s| s.ends_with(&*HOST)) != Some(true) {
            continue;
        }
        let mut fields = fields.skip(4);
        if fields.next().map(|s| s == COOKIE_NAME) != Some(true) {
            continue;
        }
        return Ok(Some(fields.next().unwrap().into()));
    }
    Ok(None)
}

fn get_marietje_host() -> String {
    match env::var("MARUSKA_HOST") {
        Ok(s) => s,
        Err(env::VarError::NotPresent) => {
            log::info!(
                "no MARUSKA_HOST variable set; defaulting to {}",
                DEFAULT_HOST,
            );
            DEFAULT_HOST.into()
        }
        Err(env::VarError::NotUnicode(_)) => {
            log::error!(
                "MARUSKA_HOST has an invalid encoding; defaulting to {}",
                DEFAULT_HOST,
            );
            DEFAULT_HOST.into()
        }
    }
}

fn referer(host: &str) -> String {
    format!("https://{}/", &host)
}

fn login_url(host: &str) -> String {
    format!("https://{}/api/login/", &host)
}

fn fetch_queue_url(host: &str) -> String {
    format!("https://{}/api/queue", &host)
}

fn load_songs_url(host: &str) -> String {
    format!("https://{}/api/songs", &host)
}

fn request_song_url(host: &str) -> String {
    format!("https://{}/api/request", &host)
}

fn skip_song_url(host: &str) -> String {
    format!("https://{}/api/skip", &host)
}

fn volume_up_url(host: &str) -> String {
    format!("https://{}/api/volumeup", &host)
}

fn volume_down_url(host: &str) -> String {
    format!("https://{}/api/volumedown", &host)
}

fn mute_url(host: &str) -> String {
    format!("https://{}/api/mute", &host)
}
