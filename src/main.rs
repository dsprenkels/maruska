mod client;
mod commands;
mod creds;
mod data;
mod help;
mod query;
mod queue;
mod search;
mod tui;

use anyhow::{format_err, Context, Error};
use lazy_static::lazy_static;
use ncurses::constants::*;
use std::convert::TryFrom;
use std::path::Path;
use std::sync::mpsc;

use data::*;

lazy_static! {
    pub static ref PROJECT_DIRS: directories::ProjectDirs =
        directories::ProjectDirs::from("nl.science", "marietje-zuid", "maruska")
            .expect("no valid home directory was found");
}

pub fn cache_dir() -> Result<&'static Path, Error> {
    static CREATE: std::sync::Once = std::sync::Once::new();
    let dir = PROJECT_DIRS.cache_dir();
    let mut result = Ok(());
    CREATE.call_once(|| {
        result = std::fs::create_dir_all(&dir);
    });
    result.with_context(|| format!("creating '{}' directory failed", dir.display()))?;
    Ok(dir)
}

fn update(model: &mut Model) {
    let event = model.tui.getch();
    queue::try_recv_queue(model);
    search::try_recv_fetched_songs(model, false);
    match event {
        // Timeout occured
        -1 => queue::update(model),
        KEY_RESIZE => model.tui.resize(),
        // Arrow keys
        KEY_UP => search::move_cursor(model, -1),
        KEY_DOWN => search::move_cursor(model, 1),
        KEY_LEFT => {}
        KEY_RIGHT => {}
        // '?'
        c if i32::from(b'?') == c => help::show_help(model),
        // Page up/down
        KEY_PPAGE => search::move_cursor(model, -25),
        KEY_NPAGE => search::move_cursor(model, 25),
        // Home/end
        KEY_HOME => search::set_cursor(model, 0),
        KEY_END => search::set_cursor(model, std::usize::MAX),
        // Backspace
        KEY_BACKSPACE | 127 => query::backspace(model),
        // Line feed
        0x0A => query::enter(model),
        // Ctrl+T
        0x14 => query::ctrl_t(model),
        // Ctrl+U
        0x15 => query::ctrl_u(model),
        // Crtl+W
        0x17 => query::ctrl_w(model),
        // Escape
        0x1B => query::escape(model),
        // Query input
        c => match char::try_from(c as u32) {
            Ok(c) => query::input(model, c),
            Err(e) => {
                let msg = format!("invalid character: {:02X} ({})", c, e);
                log::error!("{}", &msg);
                tui::set_error(model, msg);
            }
        },
    }
}

fn init() -> Model {
    // Initialize curl
    client::init();

    // Get the credentials from the user and login
    let mut login_succes = false;
    const TRIES: usize = 3;
    for _ in 0..TRIES {
        let creds = match creds::setup_credentials() {
            Ok(creds) => creds,
            Err(err) => fatal(&err.context("failed to setup credentials")),
        };
        if let Err(err) = client::Client::new().login(&creds) {
            if let Some(client::UnauthorizedError(_)) = err.downcast_ref() {
                log::warn!("login failed: unauthorized");
            } else {
                log::error!("unexpected error during login: {}", err);
                fatal(&err);
            }
            if let Err(err) = creds::purge_credentials() {
                log::warn!("error occured while purging credentials: {}", err);
            }
            continue;
        } else {
            login_succes = true;
            break;
        }
    }
    if !login_succes {
        let err = &format_err!("{} unsuccessful login attempts", TRIES);
        log::error!("{}", err);
        fatal(&err);
    }

    // Load the queue in the background
    let (fetch_queue_tx, fetch_queue_rx) = mpsc::sync_channel(0);
    std::thread::spawn(move || queue::fetch_queue_worker(fetch_queue_rx));

    // Load the list of songs in the background
    let (fetch_songs_tx, fetch_songs_rx) = mpsc::sync_channel(0);
    std::thread::spawn(move || {
        search::fetch_songs_from_filesystem(&fetch_songs_tx);
        search::fetch_songs_from_server(&fetch_songs_tx);
    });

    // Cleanup if we panic while we have curses active
    let default_hook = std::panic::take_hook();
    std::panic::set_hook(Box::new(move |panic_info| {
        ncurses::echo();
        ncurses::nocbreak();
        ncurses::endwin();
        default_hook(panic_info);
    }));

    Model {
        fetch_queue_tx: Some(fetch_queue_tx),
        fetch_songs_rx: Some(fetch_songs_rx),
        ..Default::default()
    }
}

fn main() {
    // Parse the options
    let args = std::env::args();
    let mut opts = getopts::Options::new();
    opts.optflag("h", "help", "print this help menu");
    opts.optflag("", "version", "output version information and exit");
    let matches = match opts.parse(args.skip(1)) {
        Ok(m) => m,
        Err(f) => panic!("{}", f),
    };
    if matches.opt_present("help") {
        print_usage(opts);
        return;
    }
    if matches.opt_present("version") {
        print_version();
        return;
    }

    // Initialize the logger
    // We defer this until *after* the argument parsing, in order to make the
    // cli friendlier.
    let _ = cache_dir();
    flexi_logger::Logger::with_env_or_str("info")
        .log_to_file()
        .print_message()
        .directory(PROJECT_DIRS.cache_dir())
        .suppress_timestamp()
        .start()
        .expect("could not initialize logger");
    let mut model = init();

    // Run the mainloop
    loop {
        update(&mut model);
        if model.quit {
            return;
        }

        // Draw stage
        tui::draw(&model);
    }
}

fn print_usage(opts: getopts::Options) {
    let prog = std::env::args()
        .next()
        .unwrap_or_else(|| "maruska".to_string());
    let brief = format!("Usage: {} [options]", prog);
    println!("{}", opts.usage(&brief));
}

fn print_version() {
    println!(
        "{name} {version} ({date} {commit})",
        name = option_env!("CARGO_PKG_NAME").unwrap_or("maruska"),
        version = option_env!("CARGO_PKG_VERSION").unwrap_or("[unknown version]"),
        date = option_env!("MARUSKA_DATE").unwrap_or("[unknown date]"),
        commit = option_env!("MARUSKA_COMMIT").unwrap_or("[unknown commit]"),
    );
}

fn fatal(err: &Error) -> ! {
    eprintln!("fatal error: {}", err);
    std::process::exit(1);
}
