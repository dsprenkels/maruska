use anyhow::{bail, Context, Error};
use std::fs;
use std::io::Write;
use std::os::unix::fs::PermissionsExt;
use std::path::{Path, PathBuf};
use std::process::Command;

const USERNAME_FILENAME: &str = "username.txt";
const PASSWORD_FILENAME: &str = "password.txt";

const SETUP_CREDENTIALS_SH: &str = r#"
mkdir -p "$CONFIG_DIR"
chmod 700 "$CONFIG_DIR"

if [ ! -f "$USERFILE" ]
then
        touch "$USERFILE"
        chmod 600 "$USERFILE"
        read -erp "username: " username
        echo -n "$username" >"$USERFILE"
fi

if [ ! -f "$PASSFILE" ]
then
        touch "$PASSFILE"
        chmod 600 "$PASSFILE"
        read -ersp "password: " password
        echo -n "$password" >"$PASSFILE"
fi
"#;

#[derive(serde::Serialize, serde::Deserialize)]
pub struct Credentials {
    pub username: String,
    pub password: String,
}

impl std::fmt::Debug for Credentials {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Credentials {{ username: {}, password: [redacted] }}",
            self.username
        )
    }
}

fn username_path(config_dir: &Path) -> PathBuf {
    let mut path = PathBuf::from(config_dir);
    path.push(USERNAME_FILENAME);
    path
}

fn password_path(config_dir: &Path) -> PathBuf {
    let mut path = PathBuf::from(config_dir);
    path.push(PASSWORD_FILENAME);
    path
}

pub fn setup_credentials() -> Result<Credentials, Error> {
    // Write the script to disk
    let mut script_path = std::path::PathBuf::from(crate::cache_dir()?);
    script_path.push("setup_credentials.sh");
    let mut file = fs::OpenOptions::new()
        .write(true)
        .create(true)
        .open(&script_path)
        .with_context(|| format!("opening {} failed", &script_path.display()))?;
    let mut permissions = file
        .metadata()
        .context("retrieving file metadata failed")?
        .permissions();
    permissions.set_mode(0o700);
    file.write_all(SETUP_CREDENTIALS_SH.as_bytes())?;
    file.sync_all()?;
    drop(file);

    // Execute the script
    let config_dir = crate::PROJECT_DIRS.config_dir();
    let userfile = username_path(config_dir);
    let passfile = password_path(config_dir);
    log::debug!("script path: {}", script_path.display());
    let status = Command::new("bash")
        .arg("-e")
        .arg(&script_path)
        .env("CONFIG_DIR", config_dir)
        .env("USERFILE", &userfile)
        .env("PASSFILE", &passfile)
        .status()
        .context("failed to run script")?;
    if !status.success() {
        bail!("nonzero exit status");
    }
    // Remove the script again
    fs::remove_file(script_path).context("failed to delete script")?;

    // Read back what was written to the files
    Ok(Credentials {
        username: fs::read_to_string(&userfile).expect("could not read username.txt file"),
        password: fs::read_to_string(&passfile).expect("could not read password.txt file"),
    })
}

pub fn purge_credentials() -> Result<(), Error> {
    let config_dir = crate::PROJECT_DIRS.config_dir();
    fs::remove_file(password_path(config_dir))?;
    fs::remove_file(username_path(config_dir))?;
    Ok(())
}
