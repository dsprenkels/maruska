use anyhow::Error;

pub type SongFetchResult = Result<(Vec<Song>, crate::search::SearchCache), Error>;
pub type QueueFetchResult = Result<Queue, Error>;

#[derive(Debug, Default)]
/// The application model.
///
/// Sometimes this struct is also called `App`.  It contains all the global
/// state for the complete maruska program in a singleton instance.  It is
/// common to pass a mutable reference to this singleton to functions that
/// are allowed to alter the program state.
pub struct Model {
    /// Tell the mainloop that we intend to quit as soon as possible.
    pub quit: bool,

    /// Terminal-interface-related state.
    pub tui: crate::tui::TUI,

    /// Status to show in the status bar.
    pub status: Option<(StatusPriority, String, Option<std::time::SystemTime>)>,

    /// Channel receiver on which we will receive the list of songs
    /// from the background fetcher process.
    pub fetch_songs_rx: Option<std::sync::mpsc::Receiver<SongFetchResult>>,

    /// Channel sender by which we can ask a separate thread to fetch the
    /// current queue in the background.
    pub fetch_queue_tx:
        Option<std::sync::mpsc::SyncSender<std::sync::mpsc::SyncSender<Result<Queue, Error>>>>,
    pub fetch_queue_rx: Option<std::sync::mpsc::Receiver<QueueFetchResult>>,

    /// Boolean value which is true if have loaded a list of songs.
    pub songs_loaded: bool,
    /// Database containing every song for searching.
    pub all_songs: Vec<Song>,
    /// Cache containing deunicoded+lowercase strings for tiles and artists.
    pub search_cache: crate::search::SearchCache,

    /// Stuff related to displaying the current queue.
    pub queue: Option<Queue>,
    /// The last time when the queue was fetched from the server.
    pub queue_last_fetched: Option<std::time::SystemTime>,

    /// Stuff related to querying (i.e. requesting songs).
    pub query: String,
    /// Type of query (Search/Command)
    pub query_type: crate::query::QueryType,

    // A list of pointers into `all_songs` that match the current search
    // The first usize points to the item in the respective "stub" vec,
    // the second usize points to the item in the `all_songs` vec.
    pub search_results: crate::search::SearchResults,
    /// Location of the cursor in the search results.
    pub search_cursor: usize,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
#[allow(dead_code)]
pub enum StatusPriority {
    /// A user action succeeded.
    Ok,
    /// Something happened.
    Info,
    /// Something did not go as expected and might cause problems.
    Warning,
    /// Something went wrong, and we were nog able to recover.
    Error,
}

impl StatusPriority {
    fn value(self) -> usize {
        match self {
            StatusPriority::Ok => 0,
            StatusPriority::Info => 1,
            StatusPriority::Warning => 2,
            StatusPriority::Error => 3,
        }
    }
}

impl PartialOrd for StatusPriority {
    fn partial_cmp(&self, rhs: &Self) -> std::option::Option<std::cmp::Ordering> {
        usize::partial_cmp(&self.value(), &rhs.value())
    }
}

#[derive(
    serde::Serialize,
    serde::Deserialize,
    Clone,
    Copy,
    Debug,
    Default,
    PartialEq,
    PartialOrd,
    Eq,
    Ord,
)]
/// Index into the `Model::all_songs` vec.
pub struct SongIdx(pub usize);

#[derive(
    serde::Serialize,
    serde::Deserialize,
    Clone,
    Copy,
    Debug,
    Default,
    PartialEq,
    PartialOrd,
    Eq,
    Ord,
)]
/// Index into the `Model::search_cache` vec.
pub struct StubIdx(pub usize);

#[derive(serde::Serialize, serde::Deserialize, Debug, Clone, Copy)]
pub struct Song {
    pub id: Option<i32>,
    pub artist: Option<arrayvec::ArrayString<[u8; 200]>>,
    pub title: Option<arrayvec::ArrayString<[u8; 200]>>,
    pub duration: i32,
    #[serde(rename = "uploader_name")]
    pub uploader: Option<arrayvec::ArrayString<[u8; 200]>>,
}

#[derive(serde::Serialize, serde::Deserialize, Debug)]
/// A snapshot of the current queue, fetched from the server.
pub struct Queue {
    pub current_song: QueueItem,
    pub queue: Vec<QueueItem>,
    pub started_at: i32,
    pub current_time: i32,
    pub user_name: Option<arrayvec::ArrayString<[u8; 30]>>,
}

#[derive(serde::Serialize, serde::Deserialize, Debug)]
/// A queued song, existing in `Queue`.
pub struct QueueItem {
    pub id: Option<i32>,
    pub requested_by: Option<arrayvec::ArrayString<[u8; 200]>>,
    pub song: Option<Song>,
    pub can_move_down: Option<bool>,
}
