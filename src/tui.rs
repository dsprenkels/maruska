use std::fmt::Write;

use crate::{data::*, query, search};

const TIMEOUT: std::time::Duration = std::time::Duration::from_millis(100);
const DEFAULT_STATUS_TTL: Option<std::time::Duration> = Some(std::time::Duration::from_secs(5));

// Color codes
const STYLE_NORMAL: i16 = 1;
const STYLE_RED: i16 = 2;
const STYLE_GREEN: i16 = 3;
const STYLE_YELLOW: i16 = 4;
const STYLE_BLUE: i16 = 5;

#[derive(Debug)]
/// Terminal UI state.
pub struct TUI {
    stdscr: ncurses::WINDOW,
    queue: ncurses::WINDOW,
    search: ncurses::WINDOW,
    status: ncurses::WINDOW,
}

impl Default for TUI {
    fn default() -> TUI {
        ncurses::setlocale(ncurses::LcCategory::all, "en_US.UTF-8");

        let stdscr = ncurses::initscr();
        ncurses::cbreak();
        ncurses::noecho();
        TUI::init_color();
        ncurses::keypad(stdscr, true);
        ncurses::timeout(TIMEOUT.as_millis() as i32);
        let rows = ncurses::getmaxy(stdscr);
        let cols = ncurses::getmaxx(stdscr);
        TUI {
            stdscr,
            queue: ncurses::newwin(rows - 1, cols, 0, 0),
            search: ncurses::newwin(rows - 1, cols, 0, 0),
            status: ncurses::newwin(1, cols, rows - 1, 0),
        }
    }
}

impl TUI {
    fn init_color() {
        ncurses::start_color();
        ncurses::use_default_colors();
        ncurses::init_pair(STYLE_NORMAL, -1, -1);
        ncurses::init_pair(STYLE_RED, ncurses::COLOR_RED, -1);
        ncurses::init_pair(STYLE_GREEN, ncurses::COLOR_GREEN, -1);
        ncurses::init_pair(STYLE_YELLOW, ncurses::COLOR_YELLOW, -1);
        ncurses::init_pair(STYLE_BLUE, ncurses::COLOR_BLUE, -1);
    }

    /// Wait for input from the UI.
    pub fn getch(&mut self) -> i32 {
        ncurses::getch()
    }

    /// Trigger a UI resize.
    pub fn resize(&mut self) {
        let rows = ncurses::getmaxy(self.stdscr);
        let cols = ncurses::getmaxx(self.stdscr);
        ncurses::mvwin(self.queue, 0, 0);
        ncurses::wresize(self.queue, rows - 1, cols);
        ncurses::mvwin(self.search, 0, 0);
        ncurses::wresize(self.search, rows - 1, cols);
        ncurses::mvwin(self.status, rows - 1, 0);
        ncurses::wresize(self.status, 1, cols);
    }

    /// Completely clear the ncurses screen.
    ///
    /// Use this after exiting from another command that took over the
    /// terminal.
    pub fn clear(&self) {
        ncurses::clear();
    }
}

impl Drop for TUI {
    fn drop(&mut self) {
        ncurses::delwin(self.queue);
        ncurses::delwin(self.search);
        ncurses::delwin(self.status);
        ncurses::echo();
        ncurses::nocbreak();
        ncurses::endwin();
    }
}

#[derive(Copy, Clone, Debug, Default)]
struct NCursesWriteConfig {
    /// Move to these coordinates before printing.
    coords: Option<(i32, i32)>,
    /// Print only `maxlen` characters.
    maxlen: Option<i32>,
    /// Set text attrs before printing.
    attrs: Option<ncurses::attr_t>,
    /// Align the text to the right.
    align_right: bool,
}

struct NCursesWrite {
    /// The ncurses window we should write to.
    window: ncurses::WINDOW,
    /// The options that we should use.
    cfg: NCursesWriteConfig,
}

impl std::fmt::Write for NCursesWrite {
    fn write_str(&mut self, s: &str) -> Result<(), std::fmt::Error> {
        if let Some(n) = self.cfg.maxlen {
            let slen = s.len() as i32;
            if n > slen {
                // Whole string (probably) fits
                if self.cfg.align_right {
                    for _ in 0..(n - slen) {
                        ncurses::waddch(self.window, b' ' as ncurses::chtype);
                    }
                }
                ncurses::waddnstr(self.window, s, n);
            } else {
                // Suffix with blue $
                ncurses::waddnstr(self.window, s, n - 1);
                let blue = ncurses::COLOR_PAIR(STYLE_BLUE);
                let cfg = NCursesWriteConfig {
                    attrs: Some(blue),
                    ..Default::default()
                };
                wrap_write(self.window, cfg, |wtr| wtr.write_char('$'))?;
            }

            if s.len() > n as usize {
                self.cfg.maxlen = Some(0);
                return Err(std::fmt::Error);
            } else {
                self.cfg.maxlen = Some(n - s.len() as i32);
            }
        } else {
            assert_eq!(
                self.cfg.align_right, false,
                "align_right must be used with maxlen"
            );
            ncurses::waddstr(self.window, s);
        }
        Ok(())
    }
}

/// Determine the amount of search results that are needed to completely fill
/// the search-results window.
pub fn search_results_requested_amount(model: &Model) -> usize {
    let height = ncurses::getmaxy(model.tui.search);
    compute_scroll(height as usize, model.search_cursor).saturating_add(height as usize)
}

/// Write to a window using this function
fn wrap_write<F>(
    window: ncurses::WINDOW,
    cfg: NCursesWriteConfig,
    f: F,
) -> Result<(), std::fmt::Error>
where
    F: Fn(&mut NCursesWrite) -> Result<(), std::fmt::Error>,
{
    if let Some((y, x)) = cfg.coords {
        ncurses::wmove(window, y, x);
    }
    if let Some(attrs) = cfg.attrs {
        ncurses::wattr_on(window, attrs);
    }
    let result = f(&mut NCursesWrite { window, cfg });
    if let Some(attrs) = cfg.attrs {
        ncurses::wattr_off(window, attrs);
    }
    result
}

/// Draw the UI to the screen.
pub fn draw(model: &Model) {
    if show_search_window(model) {
        draw_search(model);
    } else {
        draw_queue(model);
    }
    draw_status(model);
}

/// Show a message in the status bar.
pub fn set_status(
    model: &mut Model,
    prio: StatusPriority,
    message: String,
    ttl: Option<std::time::Duration>,
) {
    let now = std::time::SystemTime::now();
    if let Some((current_prio, _, expire)) = &model.status {
        if *current_prio > prio && expire.map_or(false, |expire| expire < now) {
            return;
        }
    }
    let expire = ttl.map(|ttl| now + ttl);
    model.status = Some((prio, message, expire));
}

/// Show an error message in the status bar.
pub fn set_error(model: &mut Model, msg: String) {
    crate::tui::set_status(model, StatusPriority::Error, msg, DEFAULT_STATUS_TTL);
}

/// Show a warning message in the status bar.
pub fn set_warning(model: &mut Model, msg: String) {
    crate::tui::set_status(model, StatusPriority::Warning, msg, DEFAULT_STATUS_TTL);
}

/// Show an "OK" message in the status bar.
pub fn set_ok(model: &mut Model, msg: String) {
    crate::tui::set_status(model, StatusPriority::Ok, msg, DEFAULT_STATUS_TTL);
}

fn show_search_window(model: &Model) -> bool {
    model.query_type == query::QueryType::Search
}

fn draw_search(model: &Model) {
    let window = model.tui.search;
    ncurses::werase(window);
    if model.songs_loaded {
        draw_search_results(model);
    } else {
        draw_search_no_results(model);
    }
    ncurses::wrefresh(window);
}

fn draw_search_no_results(model: &Model) {
    let window = model.tui.search;
    let cfg = NCursesWriteConfig {
        coords: Some((0, 0)),
        ..Default::default()
    };
    let _ = wrap_write(window, cfg, |w| w.write_char('.'));
}

fn draw_search_results(model: &Model) {
    let window = model.tui.search;
    let mut height = 0;
    let mut width = 0;
    ncurses::getmaxyx(window, &mut height, &mut width);
    let colwidths = [width / 2, width / 2];

    let mut lines_drawn = 0;
    let scroll = compute_scroll(height as usize, model.search_cursor);
    let search_results = &model.search_results.last().unwrap().results;
    let scroll_window = search_results.iter().skip(scroll).take(height as usize);
    for (y, stub_idx) in scroll_window.enumerate() {
        let song = search::get_song_by_stub_idx(model, *stub_idx).unwrap();
        let standout = scroll + y == model.search_cursor;
        if standout {
            ncurses::wattr_on(window, ncurses::A_STANDOUT());
        }
        for x in 0..width {
            let cfg = NCursesWriteConfig {
                coords: Some((y as i32, x)),
                ..Default::default()
            };
            let _ = wrap_write(window, cfg, |w| w.write_char(' '));
        }
        draw_song(window, &colwidths, y as i32, song);
        if standout {
            ncurses::wattr_off(window, ncurses::A_STANDOUT());
        }
        lines_drawn += 1;
    }
    for y in lines_drawn..height {
        draw_tilde(window, y, 0);
    }
}

/// Compute the beginning line of the scrollbox.
fn compute_scroll(height: usize, cursor: usize) -> usize {
    // Center the cursor in the middle of the screen
    cursor.saturating_sub(height / 2)
}

fn draw_song(window: ncurses::WINDOW, colwidths: &[i32; 2], y: i32, song: &Song) {
    let mut x = 0;
    for (idx, opt) in [&song.artist, &song.title].iter().enumerate() {
        let f = |wtr: &mut NCursesWrite| wtr.write_str(unwrap_or_unknown(opt));
        let cfg = NCursesWriteConfig {
            coords: Some((y, x)),
            maxlen: Some(colwidths[idx]),
            ..Default::default()
        };
        let _ = wrap_write(window, cfg, f);
        x += colwidths[idx];
    }
}

fn draw_queue(model: &Model) {
    let window = model.tui.queue;
    let mut height = 0;
    let mut width = 0;
    ncurses::getmaxyx(window, &mut height, &mut width);
    ncurses::werase(window);

    let q = match &model.queue {
        Some(q) => q,
        _ => {
            let cfg = NCursesWriteConfig {
                coords: Some((0, 0)),
                ..Default::default()
            };
            let _ = wrap_write(window, cfg, |w| w.write_char('.'));
            ncurses::wrefresh(window);
            return;
        }
    };

    // Compute the time left for each song in the queue
    let current_song = &q.current_song.song.as_ref();
    let current_song_duration = current_song.map(|s| s.duration).unwrap_or_default();

    let elapsed_since_fetch = crate::queue::time_elapsed_since_fetched(model);
    let elapsed_since_fetch = elapsed_since_fetch.unwrap_or_default().as_secs() as i32;
    let mut time_left = q.started_at + current_song_duration - q.current_time - elapsed_since_fetch;
    let mut times_left = Vec::with_capacity(q.queue.len());
    let count = (height - 1) as usize;
    for qi in q.queue.iter().take(count) {
        times_left.push(format_queue_item_time(time_left));
        time_left += qi.song.as_ref().map(|s| s.duration).unwrap_or_default();
    }

    // Layout the columns (decide the widths)
    let mut column_cell_lengths: [Vec<usize>; 4] = Default::default();
    for (qi, time_left) in q.queue.iter().zip(times_left.iter()).take(count) {
        column_cell_lengths[0].push(qi.requested_by.unwrap_or_default().len());
        if let Some(s) = &qi.song {
            column_cell_lengths[1].push(s.artist.unwrap_or_default().len());
            column_cell_lengths[2].push(s.title.unwrap_or_default().len());
        }
        column_cell_lengths[3].push(time_left.len());
    }
    let colwidths = layout_queue_columns(column_cell_lengths, width);

    // Print the current song
    draw_queue_item(window, &colwidths, 0, &q.current_song);

    // Draw the queue
    let mut y = 1;
    for (idx, qi) in q.queue.iter().take(count).enumerate() {
        draw_queue_item(window, &colwidths, y, qi);
        draw_queue_item_time(window, &colwidths, y, &times_left[idx]);
        time_left += qi.song.as_ref().map(|s| s.duration).unwrap_or_default();
        y += 1;
    }
    let lines_drawn = y;

    // Draw the tildes for empty lines
    for y in lines_drawn..height {
        draw_tilde(window, y, 0);
    }
    ncurses::wrefresh(window);
}

fn layout_queue_columns(mut column_cell_lengths: [Vec<usize>; 4], width: i32) -> [i32; 4] {
    const MIN_WIDTHS: [i32; 4] = [4, 4, 4, 6];
    for col in column_cell_lengths.iter_mut() {
        col.sort_unstable();
        col.dedup();
    }

    // Contract until everything fits
    let mut cols = [0; 4];
    loop {
        for (idx, lengths) in column_cell_lengths.iter().enumerate() {
            let slen = *lengths.last().unwrap_or(&0) as i32;
            cols[idx] = std::cmp::max(MIN_WIDTHS[idx], slen + 1); // 1 for spacing
        }
        if cols.iter().sum::<i32>() <= width {
            break;
        }
        // Remove the item that is the longest
        let (idx, _) = cols.iter().enumerate().max_by_key(|(_, x)| *x).unwrap();
        column_cell_lengths[idx].pop();
    }

    // Now expand until we fill the whole screen
    let mut idx = 0;
    while cols.iter().sum::<i32>() < width {
        cols[idx] += 1;
        // Mod 3, because we want to keep the time-column right-aligned.
        idx = (idx + 1) % 3;
    }
    cols
}

/// Print the first three column cells of a QueueItem
fn draw_queue_item(window: ncurses::WINDOW, colwidths: &[i32; 4], y: i32, qi: &QueueItem) {
    let song = &qi.song.as_ref();
    let mut x = 0;
    let cells = [
        &qi.requested_by,
        &song.and_then(|s| s.artist),
        &song.and_then(|s| s.title),
    ];
    for (idx, opt) in cells.iter().enumerate() {
        let f = |wtr: &mut NCursesWrite| wtr.write_str(unwrap_or_unknown(opt));
        let width = colwidths[idx];
        let cfg = NCursesWriteConfig {
            coords: Some((y, x)),
            maxlen: Some(width),
            ..Default::default()
        };
        let _ = wrap_write(window, cfg, f);
        x += width;
    }
}

fn draw_queue_item_time(window: ncurses::WINDOW, colwidths: &[i32; 4], y: i32, t: &str) {
    let f = |wtr: &mut NCursesWrite| wtr.write_str(t);
    let x = colwidths.iter().take(3).sum();
    let width = colwidths[3];
    let cfg = NCursesWriteConfig {
        coords: Some((y, x)),
        maxlen: Some(width),
        align_right: true,
        ..Default::default()
    };
    let _ = wrap_write(window, cfg, f);
}

/// Format an amount of seconds (given by `t`) as a string.
fn format_queue_item_time(t: i32) -> String {
    if t < 3600 {
        format!("{:>3}:{:02}", t / 60 % 60, t % 60)
    } else {
        format!("{}:{:02}:{:02}", t / 3600, t / 60 % 60, t % 60)
    }
}

/// Draw a tilde to denote the end of the list.
fn draw_tilde(window: ncurses::WINDOW, y: i32, x: i32) {
    let color = ncurses::COLOR_PAIR(STYLE_BLUE);
    let cfg = NCursesWriteConfig {
        coords: Some((y, x)),
        attrs: Some(color),
        ..Default::default()
    };
    let _ = wrap_write(window, cfg, |wtr| wtr.write_char('~'));
}

fn draw_status(model: &Model) {
    use query::QueryType::*;
    let window = model.tui.status;

    ncurses::werase(window);
    if model.query_type == Undef {
        draw_status_empty(model);
    } else {
        draw_status_query(model);
    }
    ncurses::wrefresh(window);
}

fn draw_status_empty(model: &Model) {
    let window = model.tui.status;
    if let Some((ref ty, ref message, ref ttl)) = &model.status {
        // TODO(dsprenkels) Color based on StatusPriority
        if let Some(ttl) = ttl {
            if std::time::SystemTime::now() >= *ttl {
                return;
            }
        }
        let f = |wtr: &mut NCursesWrite| write!(wtr, "{}", message);
        let width = ncurses::getmaxx(window);
        let x = std::cmp::max(0, 2 * width / 3);
        let color = ncurses::COLOR_PAIR(status_type_style(*ty));
        let cfg = NCursesWriteConfig {
            coords: Some((0, x)),
            maxlen: Some(width - x),
            attrs: Some(color | ncurses::A_BOLD()),
            ..Default::default()
        };
        let _ = wrap_write(window, cfg, f);
    }
}

fn status_type_style(ty: StatusPriority) -> i16 {
    use StatusPriority::*;
    match ty {
        Ok => STYLE_GREEN,
        Info => STYLE_BLUE,
        Warning => STYLE_YELLOW,
        Error => STYLE_RED,
    }
}

fn draw_status_query(model: &Model) {
    use query::QueryType::*;

    let f = |wtr: &mut NCursesWrite| {
        let fst = match model.query_type {
            Search => "/",
            Command => ":",
            Undef => return Ok(()),
        };
        write!(wtr, "{}{}", fst, &model.query)
    };
    let cfg = NCursesWriteConfig {
        coords: Some((0, 0)),
        ..Default::default()
    };
    let _ = wrap_write(model.tui.status, cfg, f);
}

fn unwrap_or_unknown<A>(s: &Option<arrayvec::ArrayString<A>>) -> &str
where
    A: arrayvec::Array<Item = u8> + Copy,
{
    match s {
        Some(s) => s.as_str(),
        None => "[unknown]",
    }
}
