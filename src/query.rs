use self::QueryType::*;
use crate::data::Model;

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum QueryType {
    Undef,
    Search,
    Command,
}

impl Default for QueryType {
    fn default() -> Self {
        QueryType::Undef
    }
}

/// Erase the current query string and query type.
fn reset(model: &mut Model) {
    model.query_type = Undef;
    model.query.clear();
    model.search_cursor = 0;
    crate::search::update_search_results(model);
}

/// Remove the last character from the query.
pub fn backspace(model: &mut Model) {
    if model.query.pop().is_none() {
        model.query_type = Undef;
    }
    crate::search::update_search_results(model);
}

/// Erase the current query.
pub fn ctrl_u(model: &mut Model) {
    if model.query.is_empty() {
        reset(model);
    } else {
        model.query.clear();
    }
    crate::search::update_search_results(model);
}

/// Strip the last word from the query.
pub fn ctrl_w(model: &mut Model) {
    if model.query.is_empty() {
        reset(model);
    } else {
        match model.query.trim_end().rfind(|c: char| c.is_whitespace()) {
            Some(pos) => model.query.truncate(pos + 1),
            None => model.query.clear(),
        }
    }
    crate::search::update_search_results(model);
}

/// Swap the last two characters in the query.
pub fn ctrl_t(model: &mut Model) {
    let b = model.query.pop();
    let a = model.query.pop();
    crate::search::update_search_results(model);
    if let Some(b) = b {
        model.query.push(b);
    }
    if let Some(a) = a {
        model.query.push(a);
    }
    crate::search::update_search_results(model);
}

/// Trigger a submit action.
pub fn enter(model: &mut Model) {
    match model.query_type {
        QueryType::Search => {
            crate::search::request_song(model);
            crate::queue::dispatch_fetch_queue(model);
        }
        QueryType::Command => {
            crate::commands::submit_command(model);
        }
        QueryType::Undef => {}
    }
    reset(model);
}

/// Go back to the queue window.
pub fn escape(model: &mut Model) {
    reset(model);
    crate::search::update_search_results(model);
}

/// Push a character onto the query.
pub fn input(model: &mut Model, c: char) {
    use QueryType::*;

    log::trace!("handling input character: '{}' (0x{:02X})", c, u32::from(c));
    if !c.is_ascii_alphanumeric() {
        log::debug!(
            "got a non-alphanumeric character as input: '{}' (0x{:02X})",
            c,
            u32::from(c)
        );
    }

    match (model.query_type, c) {
        (Undef, '/') => {
            model.query_type = Search;
        }
        (Undef, ':') => {
            model.query_type = Command;
        }
        (Undef, c) => {
            model.query_type = Search;
            model.query.push(c);
        }
        (_, c) => {
            model.query.push(c);
        }
    }
    crate::search::update_search_results(model);
}
