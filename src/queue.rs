use anyhow::Error;
use std::convert::TryFrom;

use crate::data::*;

const REFRESH_QUEUE_INTERVAL: std::time::Duration = std::time::Duration::from_secs(5);

/// Update the queue.
///
/// This function should be called in the mainloop whenever nothing of
/// particular interest has happened.
pub fn update(model: &mut Model) {
    if should_fetch(model) {
        dispatch_fetch_queue(model);
    }
}

/// Determine whether we should at this moment fetch the queue from the server.
fn should_fetch(model: &Model) -> bool {
    let elapsed = time_elapsed_since_fetched(model);
    if elapsed.is_none() {
        // Never fetched earlier
        return true;
    }
    let elapsed = elapsed.unwrap();
    if elapsed >= REFRESH_QUEUE_INTERVAL {
        // Previous fetch was long ago
        return true;
    }

    let queue = model.queue.as_ref();
    if queue.is_none() {
        // We did a fetch, but still don't have a valid queue.
        return true;
    }
    let queue = queue.unwrap();
    let queue_start_time = queue.started_at;
    let current_song = queue.current_song.song.as_ref();
    if current_song.is_none() {
        // We have a valid queue, but the server just returned an invalid song
        // in the `current_song` field
        log::warn!("queue.current_song.song is None");
        return false;
    }
    let current_song = current_song.unwrap();
    let elapsed = i32::try_from(elapsed.as_secs()).unwrap();
    // Compute the amount of seconds we are into playing the current song.
    let start_next_song_time = queue_start_time + current_song.duration;
    let synced_time = queue.current_time + elapsed;
    if synced_time >= start_next_song_time {
        // Current playing song has ended
        return true;
    }
    false
}

pub fn time_elapsed_since_fetched(model: &Model) -> Option<std::time::Duration> {
    if let Some(last_fetch) = model.queue_last_fetched {
        let now = std::time::SystemTime::now();
        let time_since_fetch = now.duration_since(last_fetch);
        if let Ok(elapsed) = time_since_fetch {
            return Some(elapsed);
        } else {
            log::warn!("system clock jumped backwards")
        }
    }
    None
}

/// Entrypoint of the background thread that fetches the queue.
///
/// The `rx` argument to this function should be the receiving end of the
/// channel used for requesting a fresh queue from the server.  To request a
/// new queue fetch, send a packet on the sending end of this channel.
///
/// In this packet, a channel sender is sent to the background thread, where
/// the thread will deliver the finished work.
pub fn fetch_queue_worker(
    rx: std::sync::mpsc::Receiver<std::sync::mpsc::SyncSender<Result<Queue, Error>>>,
) {
    let mut client = crate::client::Client::new();
    let mut tries = 0;
    for tx in rx.iter() {
        let queue = client.fetch_queue();
        if let Err(err) = &queue {
            log::debug!("could not fetch queue: {:?}", err);
            tries += 1;
        }
        if tx.send(queue).is_err() {
            log::warn!("cannot send queue-fetch result: other channel enpoint is not connected");
        };
        if tries >= 3 {
            client = crate::client::Client::new();
            tries = 0;
        }
    }
}

/// Instruct the background thread to fetch the queue from the server.
pub fn dispatch_fetch_queue(model: &mut Model) {
    let dispatch_tx = model
        .fetch_queue_tx
        .as_mut()
        .expect("model.fetch_queue_tx should have been initialized");
    let (tx, rx) = std::sync::mpsc::sync_channel(0);
    if dispatch_tx.try_send(tx).is_err() {
        log::trace!("cannot send a request to fetch the queue: channel is full");
        return;
    };
    model.fetch_queue_rx = Some(rx);
}

/// Try to collect some finished work from the queue-fetching background
/// thread.
///
/// If there is no new queue ready, this function will do nothing.
pub fn try_recv_queue(model: &mut Model) {
    let mut recvd_queue = None;
    if let Some(rx) = &mut model.fetch_queue_rx {
        for result in rx.try_iter() {
            match result {
                Ok(queue) => recvd_queue = Some(queue),
                Err(err) => {
                    let msg = format!("could not fetch queue: {}", err);
                    log::warn!("error: {}", msg);
                    crate::tui::set_warning(model, msg);
                    return;
                }
            }
        }
    }
    if let Some(queue) = recvd_queue {
        model.queue = Some(queue);
        model.queue_last_fetched = Some(std::time::SystemTime::now());
    }
}
