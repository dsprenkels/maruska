use anyhow::{bail, Error};
use std::str::FromStr;

use crate::data::*;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Command {
    Quit,
    Help,
    Logout,
    Skip,
    VolumeUp(u8),
    VolumeDown(u8),
    Mute,
}

impl FromStr for Command {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use Command::*;

        let mut splitn = s.split(char::is_whitespace);
        let name = splitn.next().expect("first elem of splitn cannot be None");
        match name.to_ascii_lowercase().as_ref() {
            "quit" | "q" => return Ok(Quit),
            "help" | "h" => return Ok(Help),
            "logout" => return Ok(Logout),
            "skip" => return Ok(Skip),
            "volume" => {
                if let Some(snd) = splitn.next() {
                    let mut amount = None;
                    if let Some(n) = splitn.next() {
                        amount = n.parse().ok()
                    }
                    let amount = amount.unwrap_or(1);
                    match snd.to_ascii_lowercase().as_ref() {
                        "up" => return Ok(VolumeUp(amount)),
                        "down" => return Ok(VolumeDown(amount)),
                        _ => {}
                    }
                }
            }
            "mute" => return Ok(Mute),
            _ => {}
        }
        bail!("'{}' is not a valid command", name);
    }
}

pub fn submit_command(model: &mut Model) {
    use Command::*;

    let cmd: Command = match model.query.parse() {
        Ok(cmd) => cmd,
        Err(err) => {
            crate::tui::set_error(model, err.to_string());
            return;
        }
    };

    match cmd {
        Quit => do_command_quit(model),
        Help => do_command_help(model),
        Logout => do_command_logout(model),
        Skip => do_command_skip(model),
        VolumeUp(n) => do_command_volume_up(model, n),
        VolumeDown(n) => do_command_volume_down(model, n),
        Mute => do_command_mute(model),
    }
}

fn do_command_quit(model: &mut Model) {
    model.quit = true;
}

fn do_command_help(model: &mut Model) {
    crate::help::show_help(model);
}

fn do_command_logout(model: &mut Model) {
    if let Err(err) = crate::creds::purge_credentials() {
        log::error!("could not purge credentials: {}", err);
        let msg = format!("could not purge credentials: {}", err);
        crate::tui::set_error(model, msg);
    }
}

fn do_command_skip(model: &mut Model) {
    let mut client = crate::client::Client::new();
    if let Err(err) = client.skip_song() {
        log::warn!("could not skip song: {}", err);
        let msg = format!("could not skip song: {}", err);
        crate::tui::set_error(model, msg);
        return;
    }
    crate::tui::set_ok(model, "skipped song".to_owned());
    crate::queue::dispatch_fetch_queue(model);
}

fn do_command_volume_up(model: &mut Model, n: u8) {
    let mut client = crate::client::Client::new();
    for _ in 0..n {
        if let Err(err) = client.volume_up() {
            log::warn!("could not increase volume: {}", err);
            let msg = format!("could not increase volume: {}", err);
            crate::tui::set_error(model, msg);
            return;
        }
    }
    crate::tui::set_ok(model, format!("increased volume by {}", n));
}

fn do_command_volume_down(model: &mut Model, n: u8) {
    let mut client = crate::client::Client::new();
    for _ in 0..n {
        if let Err(err) = client.volume_down() {
            log::warn!("could not decrease volume: {}", err);
            let msg = format!("could not decrease volume: {}", err);
            crate::tui::set_error(model, msg);
            return;
        }
    }
    crate::tui::set_ok(model, format!("decreased volume by {}", n));
}

fn do_command_mute(model: &mut Model) {
    let mut client = crate::client::Client::new();
    if let Err(err) = client.mute() {
        log::warn!("could not toggle mute: {}", err);
        let msg = format!("could not toggle mute: {}", err);
        crate::tui::set_error(model, msg);
        return;
    }
    crate::tui::set_ok(model, "toggled mute".to_owned());
}
